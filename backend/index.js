const express = require('express');
const fs = require('fs');
const app = express();

app.get('/locales/:lng/:ns', (req, res) => {
  const lng = req.params.lng;
  const ns = req.params.ns;
  const file = fs.readFileSync(`./locales/${lng}/${ns}.json`);
  setTimeout(() => {
    res.send(JSON.parse(file));
  }, 3000)
  
});

app.listen(3333, () => {
  console.log('Application listening on port 3333!');
});