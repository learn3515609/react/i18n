import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import Backend from 'i18next-http-backend';

i18n
  .use(initReactI18next)
  .use(Backend)
  .init({
    fallbackLng: ["ru", "en"],
    lng: "ru",
    debug: false,
    backend: {
      loadPath: 'http://localhost:3333/locales/{{lng}}/{{ns}}'
    },

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;