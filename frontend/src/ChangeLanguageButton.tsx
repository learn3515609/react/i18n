import React from 'react';
import { useTranslation } from 'react-i18next';

export const ChangeLanguageButton = () => {
  const { t, i18n } = useTranslation();
  
  const changeLanguage = () => {
    const lng = i18n.language === 'ru' ? 'en' : 'ru';
    i18n.changeLanguage(lng)
  }

  return <button onClick={changeLanguage}>{t('language.change')}</button>
}