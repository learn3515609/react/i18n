import React from 'react';
import { useTranslation } from 'react-i18next';
import { ChangeLanguageButton } from './ChangeLanguageButton';

function App() {
  const { t } = useTranslation();
  return  <>
    <h1>HEADER</h1>
    <h2>{t('Welcome to React')}</h2>
    <ChangeLanguageButton />
  </>
}

export default App;
